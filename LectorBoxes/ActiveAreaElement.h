//
//  ActiveAreaElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 29/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActiveAreaElement : NSObject
{
    int _idBoton;       //id del boton
    int _type;          //tipo de boton
    int _idCanvasElement;//id de canvas
}

@property (nonatomic,assign)int idBoton;
@property (nonatomic,assign)int type;
@property (nonatomic,assign)int idCanvasElement;

- (id)initWithidBoton:(int)idBoton type:(int)type idCanvasElement:(int)idCanvasElement;

@end