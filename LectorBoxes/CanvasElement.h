//
//  CanvasElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CanvasElement : NSObject
{
    int _id;                //id de canvas element
    int _type;              //tipo de canvas
    int _width;             //ancho de canvas
    int _height;            //altura de canvas
    int _xPosition;         //posicion en x de canvas
    int _yPosition;         //posicion en y de canvas
    int _rotationAngle;     //rotacion del angulo en canvas
    double _opacity;        //opacidad del canvas
    int _depth;             //profundidad del canvas
    int _idScene;           //id de la escena del canvas
    int _hasFunctionality;//tiene funcionalidad del canvas
}

@property (nonatomic,assign)int idCanvas;
@property (nonatomic,assign)int type;
@property (nonatomic,assign)int width;
@property (nonatomic,assign)int height;
@property (nonatomic,assign)int xPosition;
@property (nonatomic,assign)int yPosition;
@property (nonatomic,assign)int rotationAngle;
@property (nonatomic,assign)double opacity;
@property (nonatomic,assign)int depth;
@property (nonatomic,assign)int idScene;
@property (nonatomic,assign)int hasFunctionality;

-(id)initWithidCanvas:(int)idCanvas type:(int)type width:(int)width height:(int)height xPosition:(int)xPosition yPosition:(int)yPosition rotationAngle:(int)rotationAngle opacity:(double)opacity depth:(int)depth idScene:(int)idScene hasFunctionality:(int)hasFunctionality;
@end