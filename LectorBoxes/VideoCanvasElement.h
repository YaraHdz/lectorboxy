//
//  VideoCanvasElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoCanvasElement : NSObject
{
    int _idVideo;           //id dl video
    NSString *_title;       //titulo del video
    NSString *_fileName;    //nombre del archivo
    NSString *_fullPath;    //ruta del video
    NSString *_fillColor;   //color del relleno
    int _idCanvasElement;    //id de canvas
}

@property (nonatomic,assign)int idVideo;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *fileName;
@property (nonatomic,copy)NSString *fullPath;
@property (nonatomic,copy)NSString *fillColor;
@property (nonatomic,assign)int idCanvasElement;

-(id)initWithidVideo:(int)idVideo title:(NSString *)title fileName:(NSString *)fileName fullPath:(NSString *)fullPath fillColor:(NSString *)fillColor idCanvasElement:(int)idCanvasElement;

@end