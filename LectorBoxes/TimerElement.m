//
//  TimerElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "TimerElement.h"

@implementation TimerElement

@synthesize idTimer = _idTimer;
@synthesize minutes = _minutes;
@synthesize seconds = _seconds;
@synthesize idCanvasElement = _idCanvasElement;

-(id)initWithidTimer:(int)idTimer minutes:(int)minutes seconds:(int)seconds idCanvasElement:(int)idCanvasElement
{
    if ((self = [super init]))
    {
        self.idTimer = idTimer;
        self.minutes = minutes;
        self.seconds = seconds;
        self.idCanvasElement = idCanvasElement;
    }
    return self;
}

@end