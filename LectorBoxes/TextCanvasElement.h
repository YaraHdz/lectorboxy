//
//  TextCanvasElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TextCanvasElement : NSObject
{
    int _idText;                //id del text
    NSString *_textColor;       //color de texto
    int _textSize;              //tamaño del texto
    NSString *_textFont;        //fuente de texto
    NSString *_textStyle;       //estilo del texto
    NSString *_textWeight;
    NSString *_inputText;       //entrada del texto
    float _fontSpace;           //fuente de espacio
    NSString *_textAlignment;   //alineamiento del texto
    int _idCanvasElement;       //id de canvas
}

@property (nonatomic,assign)int idText;
@property (nonatomic,copy)NSString *textColor;
@property (nonatomic,assign)int textSize;
@property (nonatomic,copy)NSString *textFont;
@property (nonatomic,copy)NSString *textStyle;
@property (nonatomic,copy)NSString *textWeight;
@property (nonatomic,copy)NSString *inputText;
@property (nonatomic,assign)float fontSpace;
@property (nonatomic,copy)NSString *textAlignment;
@property (nonatomic,assign)int idCanvasElement;

-(id)initWithidText:(int)idText textColor:(NSString *)textColor textSize:(int)textSize textFont:(NSString *)textFont textStyle:(NSString *)textStyle textWeight:(NSString *)textWeight inputText:(NSString *)inputText fontSpace:(float)fontSpace textAlignment:(NSString *)textAlignment idCanvasElement:(int)idCanvasElement;

@end