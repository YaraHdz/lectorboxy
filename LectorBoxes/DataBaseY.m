//
//  DataBaseY.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 28/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "DataBaseY.h"

@implementation DataBaseY
@synthesize name = _name;

+ (DataBaseY *)creaDataBase:(NSString *)name
{
    return [[DataBaseY alloc] initWithname:name];
}

-(id)initWithname:(NSString *)name
{
    if ((self = [super init]))
    {
        self.name = name;
    
        if (sqlite3_open([name UTF8String], &ptrDatab) != SQLITE_OK)
        {
            NSLog(@"No se pudo abrir la base de datos ");
        }
    }
    return self;
}

-(void)dealloc
{
    self.name = nil;
    sqlite3_close(self->ptrDatab);
}

//Tabla de ActionButtonElement

-(NSArray *) getActionButtonElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idBoton;
    NSString *inputText;
    NSString *fillColor;
    NSString *borderColor;
    int idCanvasElement;
    
    consulta = @"SELECT _id, inputText, fillColor, borderColor, idCanvasElement FROM ActionButtonElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idBoton = sqlite3_column_int(sentencia, 0);
            inputText = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 1)];
            fillColor = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 2)];
            borderColor = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 3)];
            idCanvasElement = sqlite3_column_int(sentencia, 4);
            
            [otra addObject: [[ActionButtonElement alloc]initWithidBoton:idBoton inputText:inputText fillColor:fillColor borderColor:borderColor idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de ImageCanvasElement

-(NSArray *) getImageCanvasElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idImage;
    int type;
    NSString *name;
    NSString *fullPath;
    int idCanvasElement;
    
    consulta = @"SELECT _id, type, name, fullPath, idCanvasElement FROM ImgeCanvasElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idImage = sqlite3_column_int(sentencia, 0);
            type = sqlite3_column_int(sentencia, 1);
            name = [[NSString alloc]initWithUTF8String:(char *) sqlite3_column_text(sentencia, 2)];
            fullPath = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 3)];
            idCanvasElement = sqlite3_column_int(sentencia, 4);
            
            [otra addObject: [[ImageCanvasElement alloc]initWithidImage:idImage type:type name:name fullPath:fullPath idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de FunctionalitiesOfElement

-(NSArray *) getFunctionalitiesOfElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idFunction;
    int interactivityType;
    int correctAnswer;
    NSString *sound;
    NSString *video;
    BOOL endOfExercise;
    int goToScene;
    NSString *goToLink;
    int points;
    NSString *tooltip;
    NSString *soundFullPath;
    NSString *videoFullPath;
    int idCanvasElement;
    
    consulta = @"SELECT _id, interactivityType, correctAnswer, sound, video, endOfExercise, goToScene, goToLink, points, tooltip, soundFullPath, videoFullPath, idCanvasElement FROM FunctionalitiesOfElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idFunction = sqlite3_column_int(sentencia, 0);
            interactivityType = sqlite3_column_int(sentencia, 1);
            correctAnswer = sqlite3_column_int(sentencia, 2);
            sound = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 3)];
            video = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 4)];
            endOfExercise = sqlite3_column_int(sentencia, 5);
            goToScene = sqlite3_column_int(sentencia, 6);
            goToLink = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 7)];
            points = sqlite3_column_int(sentencia, 8);
            tooltip = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 9)];
            soundFullPath = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 10)];
            videoFullPath = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 11)];
            idCanvasElement = sqlite3_column_int(sentencia, 12);
            
            [otra addObject: [[FunctionalitiesOfElement alloc]initWithidFunction:idFunction interactivityType:interactivityType correctAnswer:correctAnswer sound:sound video:video endOfExercise:endOfExercise goToScene:goToScene goToLink:goToLink points:points tooltip:tooltip soundFullPath:soundFullPath videoFullPath:videoFullPath idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de DragDropRelationship

-(NSArray *) getDragDropRelationship
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idDragDrop;
    int idDragElement;
    int idDropElement;
    
    consulta = @"SELECT _id, idDragElement, idDropElement FROM DragDropRelationship";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idDragDrop = sqlite3_column_int(sentencia, 0);
            idDragElement = sqlite3_column_int(sentencia, 1);
            idDropElement = sqlite3_column_int(sentencia, 2);
            
            [otra addObject: [[DragDropRelationship alloc]initWithidDragDrop:idDragDrop idDragElement:idDragElement idDropElement:idDropElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de DragDropElement

-(NSArray *) getDragDropElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idDrag;
    int type;
    int idCanvasElement;
    
    consulta = @"SELECT _id, type, idCanvasElement FROM DragDrop";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idDrag = sqlite3_column_int(sentencia, 0);
            type = sqlite3_column_int(sentencia, 1);
            idCanvasElement =sqlite3_column_int(sentencia, 2);
            
            [otra addObject: [[DragDropElement alloc]initWithidDrag:idDrag type:type idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de DialogCanvasElement

-(NSArray *) getDialogCanvasElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idDialog;
    NSString *textColor;
    int textSize;
    NSString *textFont;
    NSString *textStyle;
    NSString *textWeight;
    NSString *inputText;
    NSString *fontSpace;
    NSString *textAlignment;
    int spikePosition;
    int type;
    int idCanvasElement;
    
    consulta = @"SELECT _id, textColor, textSize, textFont, textStyle, textWeight, inputText, fontSpace, textAlignment, spikePosition, type, idCanvasElement FROM DialogCanvasElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idDialog = sqlite3_column_int(sentencia, 0);
            textColor = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 1)];
            textSize = sqlite3_column_int(sentencia, 2);
            textFont = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 3)];
            textStyle = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 4)];
            textWeight = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 5)];
            inputText = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 6)];
            fontSpace = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 7)];
            textAlignment = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 8)];
            spikePosition = sqlite3_column_int(sentencia, 9);
            type = sqlite3_column_int(sentencia, 10);
            idCanvasElement = sqlite3_column_int(sentencia, 11);
            
            [otra addObject: [[DialogCanvasElement alloc]initWithidDialog:idDialog textColor:textColor textSize:textSize textFont:textFont textStyle:textStyle textWeight:textWeight inputText:inputText fontSpace:fontSpace textAlignment:textAlignment spikePosition:spikePosition type:type idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de CanvasElement

-(NSArray *) getCanvasElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idCanvas;
    int type;
    int width;
    int height;
    int xPosition;
    int yPosition;
    int rotationAngle;
    double opacity;
    int depth;
    int idScene;
    int hasFunctionality;

    consulta = @"SELECT _id, type, width, height, xPosition, yPosition, rotationAngle, opacity, depth, idScene, hasFunctionality FROM CanvasElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idCanvas = sqlite3_column_int(sentencia, 0);
            type = sqlite3_column_int(sentencia, 1);
            width = sqlite3_column_int(sentencia, 2);
            height = sqlite3_column_int(sentencia, 3);
            xPosition = sqlite3_column_int(sentencia, 4);
            yPosition = sqlite3_column_int(sentencia, 5);
            rotationAngle = sqlite3_column_int(sentencia, 6);
            opacity = sqlite3_column_double(sentencia, 7);
            depth = sqlite3_column_int(sentencia, 8);
            idScene = sqlite3_column_int(sentencia, 9);
            hasFunctionality = sqlite3_column_int(sentencia, 10);
            
            [otra addObject: [[CanvasElement alloc]initWithidCanvas:idCanvas type:type width:width height:height xPosition:xPosition yPosition:yPosition rotationAngle:rotationAngle opacity:opacity depth:depth idScene:idScene hasFunctionality:hasFunctionality]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de Box

-(NSArray *) getBox
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idBox;
    int version;
    
    consulta = @"SELECT _id, version FROM Box";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idBox = sqlite3_column_int(sentencia, 0);
            version = sqlite3_column_int(sentencia, 1);
            
            [otra addObject: [[Box alloc]initWithidBox:idBox version:version]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de VideoCanvasElement

-(NSArray *) getVideoCanvasElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idVideo;
    NSString *title;
    NSString *fileName;
    NSString *fullPath;
    NSString *fillColor;
    int idCanvasElement;
    
    consulta = @"SELECT _id, title, filename, fullPath, fillColor, idCanvasElement FROM VideoCanvasElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idVideo = sqlite3_column_int(sentencia, 0);
            title = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 1)];
            fileName = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 2)];
            fullPath = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 3)];
            fillColor = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 4)];
            idCanvasElement = sqlite3_column_int(sentencia, 5);
            
            [otra addObject: [[VideoCanvasElement alloc]initWithidVideo:idVideo title:title fileName:fileName fullPath:fullPath fillColor:fillColor idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de TimerElement

-(NSArray *) getTimerElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idTimer;
    int minutes;
    int seconds;
    int idCanvasElement;
    
    consulta = @"SELECT _id, minutes, seconds, idCanvasElement FROM TimerElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idTimer = sqlite3_column_int(sentencia, 0);
            minutes = sqlite3_column_int(sentencia, 1);
            seconds = sqlite3_column_int(sentencia, 2);
            idCanvasElement = sqlite3_column_int(sentencia, 3);
            
            [otra addObject: [[TimerElement alloc]initWithidTimer:idTimer minutes:minutes seconds:seconds idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de TextCanvasElement

-(NSArray *) getTextCanvasElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idText;
    NSString *textColor;
    int textSize;
    NSString *textFont;
    NSString *textStyle;
    NSString *textWeight;
    NSString *inputText;
    float fontSpace;
    NSString *textAlignment;
    int idCanvasElement;
    
    consulta = @"SELECT _id, textColor, textSize, textFont, textStyle, textWeight, inputText, fontSpace, textAlignment, idCanvasElement FROM TextCanvasElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idText = sqlite3_column_int(sentencia, 0);
            textColor = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 1)];
            textSize = sqlite3_column_int(sentencia, 2);
            textFont = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 3)];
            textStyle = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 4)];
            textWeight = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 5)];
            inputText = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 6)];
            fontSpace = (float)sqlite3_column_double(sentencia, 7);
            textAlignment = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 8)];
            idCanvasElement = sqlite3_column_int(sentencia, 9);
            
            [otra addObject: [[TextCanvasElement alloc]initWithidText:idText textColor:textColor textSize:textSize textFont:textFont textStyle:textStyle textWeight:textWeight inputText:inputText fontSpace:fontSpace textAlignment:textAlignment idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de ShapeCanvasElement

-(NSArray *) getShapeCanvasElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idShape;
    NSString *fillColor;
    NSString *strokeColor;
    int strokeSize;
    int shapeType;
    int idCanvasElement;
    
    consulta = @"SELECT _id, fillColor, strokeColor, strokeSize, shapeType, idCanvasElement FROM ShapeCanvasElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idShape = sqlite3_column_int(sentencia, 0);
            fillColor = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 1)];
            strokeColor = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 2)];
            strokeSize = sqlite3_column_int(sentencia, 3);
            shapeType = sqlite3_column_int(sentencia, 4);
            idCanvasElement = sqlite3_column_int(sentencia, 5);
            
            [otra addObject: [[ShapeCanvasElement alloc]initWithidShape:idShape fillColor:fillColor strokeColor:strokeColor strokeSize:strokeSize shapeType:shapeType idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de Scene

-(NSArray *) getScene
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idScene;
    int sceneNumber;
    NSString *sound;
    NSString *suggestions;
    NSString *background;
    NSString *soundFullPath;
    int numberCorrect;
    
    consulta = @"SELECT _id, sceneNumber, sound, suggestions, background, soundFullPath, numberCorrect FROM Scene";
    
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idScene = sqlite3_column_int(sentencia, 0);
            sceneNumber = sqlite3_column_int(sentencia, 1);
            sound = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 2)];
            suggestions = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 3)];
            background = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 4)];
            soundFullPath = [[NSString alloc]initWithUTF8String:(char *)sqlite3_column_text(sentencia, 5)];
            numberCorrect = sqlite3_column_int(sentencia, 6);
            
            [otra addObject: [[Scene alloc]initWithidScene:idScene sceneNumber:sceneNumber sound:sound suggestions:suggestions background:background soundFullPath:soundFullPath numberCorrect:numberCorrect]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

//Tabla de ActiveAreaElement

-(NSArray *) getActiveAreaElement
{
    NSMutableArray *otra;
    NSString *consulta;
    sqlite3_stmt *sentencia;
    int idBoton;
    int type;
    int idCanvasElement;
    
    consulta = @"SELECT _id, type, idCanvasElement FROM ActiveAreaElement";
    otra = [[NSMutableArray alloc]init];
    
    if (sqlite3_prepare_v2(ptrDatab, [consulta UTF8String], -1, &sentencia, nil) == SQLITE_OK)
    {
        while (sqlite3_step(sentencia) == SQLITE_ROW)
        {
            idBoton = sqlite3_column_int(sentencia, 0);
            type = sqlite3_column_int(sentencia, 1);
            idCanvasElement = sqlite3_column_int(sentencia, 2);
            
            [otra addObject: [[ActiveAreaElement alloc]initWithidBoton:idBoton type:type idCanvasElement:idCanvasElement]];
        }
        sqlite3_finalize(sentencia);
    }
    return otra;
}

@end