//
//  FunctionalitiesOfElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FunctionalitiesOfElement : NSObject
{
    int _idFunction;            //id de la funcion
    int _interactivityType;     //tipo de interactividad
    int _correctAnswer;         //respuesta correcta
    NSString *_sound;           //sonido de la funcionalidad
    NSString *_video;           //video de la funcionalidad
    BOOL _endOfExercise;        //fin del ejercicio
    int _goToScene;             //ir a la escena
    NSString *_goToLink;        //ir al link
    int _points;                //puntos
    NSString *_tooltip;         //informacion de las herramientas
    NSString *_soundFullPath;   //sonido de la ruta
    NSString *_videoFullPath;   //video de la ruta
    int _idCanvasElement;       //id de canvas
}

@property (nonatomic,assign)int idFunction;
@property (nonatomic,assign)int interactivityType;
@property (nonatomic,assign)int correctAnswer;
@property (nonatomic,copy)NSString *sound;
@property (nonatomic,copy)NSString *video;
@property (nonatomic,assign,getter=isendOfExercise)BOOL endOfExercise;
@property (nonatomic,assign)int goToScene;
@property (nonatomic,copy)NSString *goToLink;
@property (nonatomic,assign)int points;
@property (nonatomic,copy)NSString *tooltip;
@property (nonatomic,copy)NSString *soundFullPath;
@property (nonatomic,copy)NSString *videoFullPath;
@property (nonatomic,assign)int idCanvasElement;

-(id)initWithidFunction:(int)idFunction interactivityType:(int)interactivityType correctAnswer:(int)correctAnswer sound:(NSString *)sound video:(NSString *)video endOfExercise:(BOOL)endOfExercise goToScene:(int)goToScene goToLink:(NSString *)goToLink points:(int)points tooltip:(NSString *)tooltip soundFullPath:(NSString *)soundFullPath videoFullPath:(NSString *)videoFullPath idCanvasElement:(int)idCanvasElement;

@end