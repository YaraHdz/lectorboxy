//
//  Box.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "Box.h"

@implementation Box
@synthesize idBox = _idBox;
@synthesize version = _version;


-(id)initWithidBox:(int)idBox version:(int)version
{
    if ((self = [super init]))
    {
        self.idBox = idBox;
        self.version = version;
    }
    return self;
}

@end