//
//  CanvasElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "CanvasElement.h"

@implementation CanvasElement
@synthesize idCanvas = _id;
@synthesize type = _type;
@synthesize width = _width;
@synthesize height = _height;
@synthesize xPosition = _xPosition;
@synthesize yPosition = _yPosition;
@synthesize rotationAngle = _rotationAngle;
@synthesize opacity = _opacity;
@synthesize depth = _depth;
@synthesize idScene = _idScene;
@synthesize hasFunctionality = _hasFunctionality;

-(id)initWithidCanvas:(int)idCanvas type:(int)type width:(int)width height:(int)height xPosition:(int)xPosition yPosition:(int)yPosition rotationAngle:(int)rotationAngle opacity:(double)opacity depth:(int)depth idScene:(int)idScene hasFunctionality:(int)hasFunctionality
{
    if ((self = [super init]))
    {
        self.idCanvas = idCanvas;
        self.type = type;
        self.width = width;
        self.height = height;
        self.xPosition = xPosition;
        self.yPosition = yPosition;
        self.rotationAngle = rotationAngle;
        self.opacity = opacity;
        self.depth = depth;
        self.idScene = idScene;
        self.hasFunctionality = hasFunctionality;
    }
    return self;
}

@end