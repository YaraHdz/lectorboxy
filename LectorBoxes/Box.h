//
//  Box.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Box : NSObject
{
    int _idBox;     //id del box
    int _version;   //version del box
}

@property (nonatomic,assign)int idBox;
@property (nonatomic,assign)int version;

-(id)initWithidBox:(int)idBox version:(int)version;

@end