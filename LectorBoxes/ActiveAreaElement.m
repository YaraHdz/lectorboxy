//
//  ActiveAreaElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 29/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "ActiveAreaElement.h"

@implementation ActiveAreaElement

@synthesize idBoton = _idBoton;
@synthesize type = _type;
@synthesize idCanvasElement = _idCanvasElement;

- (id)initWithidBoton:(int)idBoton type:(int)type idCanvasElement:(int)idCanvasElement
{
    if ((self = [super init]))
    {
        self.idBoton = idBoton;
        self.type = type;
        self.idCanvasElement = idCanvasElement;
     }
    return self;
}

@end