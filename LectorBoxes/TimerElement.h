//
//  TimerElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimerElement : NSObject
{
    int _idTimer;           //id del timer
    int _minutes;           //minutos del timer
    int _seconds;           //segundos del timer
    int _idCanvasElement;   //idde canvas
}

@property (nonatomic,assign)int idTimer;
@property (nonatomic,assign)int minutes;
@property (nonatomic,assign)int seconds;
@property (nonatomic,assign)int idCanvasElement;

-(id)initWithidTimer:(int)idTimer minutes:(int)minutes seconds:(int)seconds idCanvasElement:(int)idCanvasElement;

@end