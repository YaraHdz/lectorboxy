//
//  Scene.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Scene : NSObject
{
    int _idScene;               //id de la escena
    int _sceneNumber;           //numeo de la escena
    NSString *_sound;           //sonido de la escena
    NSString *_suggestions;     //sugerencias de la escena
    NSString *_background;      //fondo de la escena
    NSString *_soundFullPath;   //sonido de la ruta
    int _numberCorrect;         //numero de respuestas correctas
}

@property (nonatomic,assign)int idScene;
@property (nonatomic,assign)int sceneNumber;
@property (nonatomic,copy)NSString *sound;
@property (nonatomic,copy)NSString *suggestions;
@property (nonatomic,copy)NSString *background;
@property (nonatomic,copy)NSString *soundFullPath;
@property (nonatomic,assign)int numberCorrect;

-(id)initWithidScene:(int)idScene sceneNumber:(int)sceneNumber sound:(NSString *)sound suggestions:(NSString *)suggestions background:(NSString *)background soundFullPath:(NSString *)soundFullPath numberCorrect:(int)numberCorrect;

@end