//
//  ShapeCanvasElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "ShapeCanvasElement.h"

@implementation ShapeCanvasElement
@synthesize idShape = _idShape;
@synthesize fillColor = _fillColor;
@synthesize strokeColor = _strokeColor;
@synthesize strokeSize = _strokeSize;
@synthesize shapeType = _shapeType;
@synthesize idCanvasElement = _idCanvasElement;

-(id)initWithidShape:(int)idShape fillColor:(NSString *)fillColor strokeColor:(NSString *)strokeColor strokeSize:(int)strokeSize shapeType:(int)shapeType idCanvasElement:(int)idCanvasElement
{
    if((self = [super init]))
    {
        self.idShape = idShape;
        self.fillColor = fillColor;
        self.strokeColor = strokeColor;
        self.strokeSize = strokeSize;
        self.shapeType = shapeType;
        self.idCanvasElement =idCanvasElement;
    }
    return self;
}

-(void)dealloc
{
    self.fillColor = nil;
    self.strokeColor = nil;
}

@end