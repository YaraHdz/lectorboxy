//
//  ActionButtonElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "ActionButtonElement.h"
@import AppKit;

@implementation ActionButtonElement
@synthesize idBoton = _id;
@synthesize inputText = _inputText;
@synthesize fillColor = _fillColor;
@synthesize borderColor = _borderColor;
@synthesize idCanvasElement = _idCanvasElement;

-(id)initWithidBoton:(int)idBoton inputText:(NSString *)inputText fillColor:(NSString *)fillColor borderColor:(NSString *)borderColor idCanvasElement:(int)idCanvasElement
{
    if ((self = [super init]))
    {
        self.idBoton = idBoton;
        self.inputText = inputText;
        self.fillColor = fillColor;
        self.borderColor = borderColor;
        self.idCanvasElement = idCanvasElement;
    }
    return self;
}

-(void)dealloc
{
    self.inputText = nil;
    self.fillColor = nil;
    self.borderColor = nil;
}

@end