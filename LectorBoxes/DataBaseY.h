//
//  DataBaseY.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 28/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "ActionButtonElement.h"
#import "ActiveAreaElement.h"
#import "Box.h"
#import "CanvasElement.h"
#import "DialogCanvasElement.h"
#import "DragDropElement.h"
#import "DragDropRelationship.h"
#import "FunctionalitiesOfElement.h"
#import "ImageCanvasElement.h"
#import "Scene.h"
#import "ShapeCanvasElement.h"
#import "TextCanvasElement.h"
#import "TimerElement.h"
#import "VideoCanvasElement.h"


@interface DataBaseY : NSObject
{
    sqlite3 *ptrDatab;
    NSString *_name;
}

@property (nonatomic,copy)NSString *name;

+ (DataBaseY *)creaDataBase:(NSString *)name;

- (NSArray *)getActionButtonElement;
- (NSArray *)getImageCanvasElement;
- (NSArray *)getFunctionalitiesOfElement;
- (NSArray *)getDragDropRelationship;
- (NSArray *)getDragDropElement;
- (NSArray *)getDialogCanvasElement;
- (NSArray *)getCanvasElement;
- (NSArray *)getBox;
- (NSArray *)getActiveAreaElement;
- (NSArray *)getVideoCanvasElement;
- (NSArray *)getTimerElement;
- (NSArray *)getTextCanvasElement;
- (NSArray *)getShapeCanvasElement;
- (NSArray *)getScene;

-(id)initWithname:(NSString *)name;

@end