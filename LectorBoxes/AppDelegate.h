//
//  AppDelegate.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 28/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DataBaseY.h"

@interface AppDelegate : NSObject <NSApplicationDelegate, NSTableViewDataSource, NSTableViewDelegate, NSTabViewDelegate>
{
    NSArray *cambiaArr;
    DataBaseY *conexDataBase;
}

- (IBAction)AbreArchivo:(NSMenuItem *)sender;
@property (weak) IBOutlet NSTableView *impActive;
@property (weak) IBOutlet NSTableView *impBox;
@property (weak) IBOutlet NSTableView *impCanvas;
@property (weak) IBOutlet NSTableView *impDDE;
@property (weak) IBOutlet NSTableView *impDDR;
@property (weak) IBOutlet NSTableView *impDialog;
@property (weak) IBOutlet NSTableView *impFunction;
@property (weak) IBOutlet NSTableView *impImage;
@property (weak) IBOutlet NSTableView *impScene;
@property (weak) IBOutlet NSTableView *impShape;
@property (weak) IBOutlet NSTableView *impText;
@property (weak) IBOutlet NSTableView *impTimer;
@property (weak) IBOutlet NSTableView *impVideo;
@property (weak) IBOutlet NSTabView *tvRefe;
@property (weak) IBOutlet NSTableView *impAButton;

@end