//
//  ShapeCanvasElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShapeCanvasElement : NSObject
{
    int _idShape;           //id de la forma
    NSString *_fillColor;   //color de relleno
    NSString *_strokeColor; //color de stroke
    int _strokeSize;        //tamaño de stroke
    int _shapeType;         //tipo de la forma
    int _idCanvasElement;   //id de canvas
}

@property (nonatomic,assign)int idShape;
@property (nonatomic,copy)NSString *fillColor;
@property (nonatomic,copy)NSString *strokeColor;
@property (nonatomic,assign)int strokeSize;
@property (nonatomic,assign)int shapeType;
@property (nonatomic,assign)int idCanvasElement;

-(id)initWithidShape:(int)idShape fillColor:(NSString *)fillColor strokeColor:(NSString *)strokeColor strokeSize:(int)strokeSize shapeType:(int)shapeType idCanvasElement:(int)idCanvasElement;

@end