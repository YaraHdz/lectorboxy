//
//  DialogCanvasElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DialogCanvasElement : NSObject
{
    int _idDialog;          //id del dialogo
    NSString *_textColor;   //color del texto
    int _textSize;          //tamaño del texto
    NSString *_textFont;    //fuente del texto
    NSString *_textStyle;   //estilo del texto
    NSString *_textWeight;  //ancho del texto
    NSString *_inputText;   //entrada del texto
    NSString *_fontSpace;   //fuente de espacio
    NSString *_textAlignment;//alineamiento del texto
    int _spikePosition;     //posicion de la punta
    int _type;              //tipo de dialogo
    int _idCanvasElement;   //id del canvas
}

@property (nonatomic,assign)int idDialog;
@property (nonatomic,copy)NSString *textColor;
@property (nonatomic,assign)int textSize;
@property (nonatomic,copy)NSString *textFont;
@property (nonatomic,copy)NSString *textStyle;
@property (nonatomic,copy)NSString *textWeight;
@property (nonatomic,copy)NSString *inputText;
@property (nonatomic,copy)NSString *fontSpace;
@property (nonatomic,copy)NSString *textAlignment;
@property (nonatomic,assign)int spikePosition;
@property (nonatomic,assign)int type;
@property (nonatomic,assign)int idCanvasElement;

-(id)initWithidDialog:(int)idDialog textColor:(NSString *)textColor textSize:(int)textSize textFont:(NSString *)textFont textStyle:(NSString *)textStyle textWeight:(NSString *)textWeight inputText:(NSString *)inputText fontSpace:(NSString *)fontSpace textAlignment:(NSString *)textAlignment spikePosition:(int)spikePosition type:(int)type idCanvasElement:(int)idCanvasElement;

@end