//
//  AppDelegate.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 28/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()
@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    // Insert code here to tear down your application
}

- (IBAction)AbreArchivo:(NSMenuItem *)sender
{
    NSOpenPanel *panel;

    panel = [NSOpenPanel openPanel];
    [panel setCanChooseDirectories:NO];
    [panel setCanChooseFiles:YES];
    [panel setAllowsMultipleSelection:NO];
    [panel setAllowedFileTypes:[NSArray arrayWithObject:@"box"]];
    [panel setCanCreateDirectories:YES];
    [panel setCanHide:NO];
        
    if ( [panel runModal] == NSModalResponseOK)
    {
        NSURL *unaRuta = [[panel URLs]objectAtIndex:0];
        NSString *unaCadena;
        unaCadena = [NSString stringWithFormat:@"%@", unaRuta];
        printf("%s", [unaCadena UTF8String]);
        conexDataBase = [DataBaseY creaDataBase:unaCadena];
    }
}

-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    NSInteger otro = 0;
    
    if (cambiaArr != nil)
    {
        otro = [cambiaArr count];
    }
    return otro;
}

-(id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSDictionary *dicc = [cambiaArr objectAtIndex:row];
    NSString *cIdentificador = [tableColumn identifier];
    
    return [NSString stringWithFormat:@"%@", [dicc valueForKey:cIdentificador]];
}

-(void)tabView:(NSTabView *)tabView didSelectTabViewItem:(NSTabViewItem *)tabViewItem
{
    NSTabViewItem *pActual;
    pActual = self.tvRefe.selectedTabViewItem;
    
    switch ([[pActual identifier] intValue])
    {
        case 1:
            cambiaArr = [conexDataBase getActiveAreaElement];
            [self.impActive reloadData];
            break;
            
        case 2:
            cambiaArr = [conexDataBase getActionButtonElement];
            [self.impAButton reloadData];
            break;
            
        case 3:
            cambiaArr = [conexDataBase getBox];
            [self.impBox reloadData];
            break;
            
        case 4:
            cambiaArr = [conexDataBase getCanvasElement];
            [self.impCanvas reloadData];
            break;
            
        case 5:
            cambiaArr = [conexDataBase getDragDropElement];
            [self.impDDE reloadData];
            break;
            
        case 6:
            cambiaArr = [conexDataBase getDragDropRelationship];
            [self.impDDR reloadData];
            break;
            
        case 7:
            cambiaArr = [conexDataBase getDialogCanvasElement];
            [self.impDialog reloadData];
            break;
            
        case 8:
            cambiaArr = [conexDataBase getFunctionalitiesOfElement];
            [self.impFunction reloadData];
            break;
            
        case 9:
            cambiaArr = [conexDataBase getImageCanvasElement];
            [self.impImage reloadData];
            break;
            
        case 10:
            cambiaArr = [conexDataBase getScene];
            [self.impScene reloadData];
            break;
            
        case 11:
            cambiaArr = [conexDataBase getShapeCanvasElement];
            [self.impShape reloadData];
            break;
            
        case 12:
            cambiaArr = [conexDataBase getTextCanvasElement];
            [self.impText reloadData];
            break;
            
        case 13:
            cambiaArr = [conexDataBase getTimerElement];
            [self.impTimer reloadData];
            break;
            
        case 14:
            cambiaArr = [conexDataBase getVideoCanvasElement];
            [self.impVideo reloadData];
            break;
        default:
            break;
    }
}

@end