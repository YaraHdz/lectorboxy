//
//  DragDropElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "DragDropElement.h"

@implementation DragDropElement
@synthesize idDrag = _id;
@synthesize type = _type;
@synthesize idCanvasElement = _idCanvasElement;

-(id)initWithidDrag:(int)idDrag type:(int)type idCanvasElement:(int)idCanvasElement
{
    if ((self = [super init]))
    {
        self.idDrag = idDrag;
        self.type = type;
        self.idCanvasElement = idCanvasElement;
    }
    return self;
}

@end