//
//  FunctionalitiesOfElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "FunctionalitiesOfElement.h"

@implementation FunctionalitiesOfElement
@synthesize idFunction = _idFunction;
@synthesize interactivityType = _interactivityType;
@synthesize correctAnswer = _correctAnswer;
@synthesize sound = _sound;
@synthesize video = _video;
@synthesize endOfExercise = _endOfExercise;
@synthesize goToScene = _goToScene;
@synthesize goToLink = _goToLink;
@synthesize points = _points;
@synthesize tooltip = _tooltip;
@synthesize soundFullPath = _soundFullPath;
@synthesize videoFullPath = _videoFullPath;
@synthesize idCanvasElement = _idCanvasElement;

-(id)initWithidFunction:(int)idFunction interactivityType:(int)interactivityType correctAnswer:(int)correctAnswer sound:(NSString *)sound video:(NSString *)video endOfExercise:(BOOL)endOfExercise goToScene:(int)goToScene goToLink:(NSString *)goToLink points:(int)points tooltip:(NSString *)tooltip soundFullPath:(NSString *)soundFullPath videoFullPath:(NSString *)videoFullPath idCanvasElement:(int)idCanvasElement
{
    if ((self = [super init]))
    {
        self.idFunction = idFunction;
        self.interactivityType = interactivityType;
        self.correctAnswer = correctAnswer;
        self.sound = sound;
        self.video = video;
        self.endOfExercise = endOfExercise;
        self.goToScene = goToScene;
        self.goToLink = goToLink;
        self.points = points;
        self.tooltip = tooltip;
        self.soundFullPath = soundFullPath;
        self.videoFullPath = videoFullPath;
        self.idCanvasElement = idCanvasElement;
    }
    return self;
}

-(void)dealloc
{
    self.sound = nil;
    self.video = nil;
    self.goToLink = nil;
    self.tooltip = nil;
    self.soundFullPath = nil;
    self.videoFullPath = nil;
}

@end