//
//  DragDropElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DragDropElement : NSObject
{
    int _id;                //id del DragDrop
    int _type;              //tipo de DragDrop
    int _idCanvasElement;   //id del canvas
}

@property (nonatomic,assign)int idDrag;
@property (nonatomic,assign)int type;
@property (nonatomic,assign)int idCanvasElement;

-(id)initWithidDrag:(int)idDrag type:(int)type idCanvasElement:(int)idCanvasElement;

@end