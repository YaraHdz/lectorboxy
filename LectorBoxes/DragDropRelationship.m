//
//  DragDropRelationship.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "DragDropRelationship.h"

@implementation DragDropRelationship
@synthesize idDragDrop = _id;
@synthesize idDragElement = _idDragElement;
@synthesize idDropElement = _idDropElement;

-(id)initWithidDragDrop:(int)idDragDrop idDragElement:(int)idDragElement idDropElement:(int)idDropElement
{
    if ((self = [super init]))
    {
        self.idDragDrop = idDragDrop;
        self.idDragElement = idDragElement;
        self.idDropElement = idDropElement;
    }
    return self;
}

@end