//
//  DialogCanvasElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "DialogCanvasElement.h"

@implementation DialogCanvasElement
@synthesize idDialog = _idDialog;
@synthesize textColor = _textColor;
@synthesize textSize = _textSize;
@synthesize textFont = _textFont;
@synthesize textStyle = _textStyle;
@synthesize textWeight = _textWeight;
@synthesize inputText = _inputText;
@synthesize fontSpace = _fontSpace;
@synthesize textAlignment = _textAlignment;
@synthesize spikePosition = _spikePosition;
@synthesize type = _type;
@synthesize idCanvasElement = _idCanvasElement;

-(id)initWithidDialog:(int)idDialog textColor:(NSString *)textColor textSize:(int)textSize textFont:(NSString *)textFont textStyle:(NSString *)textStyle textWeight:(NSString *)textWeight inputText:(NSString *)inputText fontSpace:(NSString *)fontSpace textAlignment:(NSString *)textAlignment spikePosition:(int)spikePosition type:(int)type idCanvasElement:(int)idCanvasElement
{
    if ((self = [super init]))
    {
        self.idDialog = idDialog;
        self.textColor = textColor;
        self.textSize = textSize;
        self.textFont = textFont;
        self.textStyle = textStyle;
        self.textWeight = textWeight;
        self.inputText = inputText;
        self.fontSpace = fontSpace;
        self.textAlignment = textAlignment;
        self.spikePosition = spikePosition;
        self.type = type;
        self.idCanvasElement = idCanvasElement;
    }
    return self;
}

-(void)dealloc
{
    self.textColor = nil;
    self.textFont = nil;
    self.textStyle = nil;
    self.textWeight = nil;
    self.inputText = nil;
    self.fontSpace = nil;
    self.textAlignment = nil;
}

@end