//
//  Scene.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "Scene.h"

@implementation Scene
@synthesize idScene= _idScene;
@synthesize sceneNumber = _sceneNumber;
@synthesize sound = _sound;
@synthesize suggestions = _suggestions;
@synthesize background = _background;
@synthesize soundFullPath = _soundFullPath;
@synthesize numberCorrect = _numberCorrect;

-(id)initWithidScene:(int)idScene sceneNumber:(int)sceneNumber sound:(NSString *)sound suggestions:(NSString *)suggestions background:(NSString *)background soundFullPath:(NSString *)soundFullPath numberCorrect:(int)numberCorrect
{
    if ((self = [super init]))
    {
        self.idScene = idScene;
        self.sceneNumber = sceneNumber;
        self.sound = sound;
        self.suggestions = suggestions;
        self.background = background;
        self.soundFullPath = soundFullPath;
        self.numberCorrect = numberCorrect;
    }
    return self;
}

-(void)dealloc
{
    self.sound = nil;
    self.suggestions = nil;
    self.background = nil;
    self.soundFullPath = nil;
}

@end