//
//  ActionButtonElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 23/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActionButtonElement : NSObject
{
    int _id;                //id del boton
    NSString *_inputText;   //introducir texto
    NSString *_fillColor;   //Color de relleno
    NSString *_borderColor; //color del contorno
    int _idCanvasElement;   //id de canvas
}

@property (nonatomic, assign)int idBoton;
@property (nonatomic,copy)NSString *inputText;
@property (nonatomic,copy)NSString *fillColor;
@property (nonatomic,copy)NSString *borderColor;
@property (nonatomic,assign)int idCanvasElement;

- (id)initWithidBoton:(int)idBoton inputText:(NSString *)inputText fillColor:(NSString *)fillColor borderColor:(NSString *)borderColor idCanvasElement:(int)idCanvasElement;

@end