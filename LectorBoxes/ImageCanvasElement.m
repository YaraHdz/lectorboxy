//
//  ImageCanvasElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "ImageCanvasElement.h"

@implementation ImageCanvasElement
@synthesize idImage = _idImage;
@synthesize type = _type;
@synthesize name = _name;
@synthesize fullPath = _fullPath;
@synthesize idCanvasElement = _idCanvasElement;

-(id)initWithidImage:(int)idImage type:(int)type name:(NSString *)name fullPath:(NSString *)fullPath idCanvasElement:(int)idCanvasElement
{
    if ((self = [super init]))
    {
        self.idImage = idImage;
        self.type = type;
        self.name = name;
        self.fullPath = fullPath;
        self.idCanvasElement = idCanvasElement;
    }
    return self;
}

-(void)dealloc
{
    self.name = nil;
    self.fullPath = nil;
}

@end