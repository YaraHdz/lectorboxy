//
//  TextCanvasElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "TextCanvasElement.h"

@implementation TextCanvasElement

@synthesize idText = _idText;
@synthesize textColor = _textColor;
@synthesize textSize = _textSize;
@synthesize textFont = _textFont;
@synthesize textWeight = _textWeight;
@synthesize inputText = _inputText;
@synthesize fontSpace = _fontSpace;
@synthesize textAlignment = _textAlignment;
@synthesize idCanvasElement = _idCanvasElement;

-(id)initWithidText:(int)idText textColor:(NSString *)textColor textSize:(int)textSize textFont:(NSString *)textFont textStyle:(NSString *)textStyle textWeight:(NSString *)textWeight inputText:(NSString *)inputText fontSpace:(float)fontSpace textAlignment:(NSString *)textAlignment idCanvasElement:(int)idCanvasElement
{
    if ((self = [super init]))
    {
        self.idText = idText;
        self.textColor = textColor;
        self.textSize = textSize;
        self.textFont = textFont;
        self.textWeight = textWeight;
        self.inputText = inputText;
        self.fontSpace = fontSpace;
        self.textAlignment = textAlignment;
        self.idCanvasElement = idCanvasElement;
    }
    return self;
}

-(void)dealloc
{
    self.textColor = nil;
    self.textFont = nil;
    self.textWeight = nil;
    self.inputText = nil;
    self.textAlignment = nil;
}

@end