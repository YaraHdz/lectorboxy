//
//  VideoCanvasElement.m
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import "VideoCanvasElement.h"

@implementation VideoCanvasElement

@synthesize idVideo = _idVideo;
@synthesize title = _title;
@synthesize fileName = _fileName;
@synthesize fullPath = _fullPath;
@synthesize fillColor = _fillColor;
@synthesize idCanvasElement = _idCanvasElement;

-(id)initWithidVideo:(int)idVideo title:(NSString *)title fileName:(NSString *)fileName fullPath:(NSString *)fullPath fillColor:(NSString *)fillColor idCanvasElement:(int)idCanvasElement
{
    if ((self = [super init]))
    {
        self.idVideo = idVideo;
        self.title = title;
        self.fileName = fileName;
        self.fullPath = fullPath;
        self.fillColor = fillColor;
        self.idCanvasElement = idCanvasElement;
    }
    return self;
}

-(void) dealloc
{
    self.title = nil;
    self.fileName = nil;
    self.fileName = nil;
    self.fullPath = nil;
    self.fillColor = nil;
}

@end