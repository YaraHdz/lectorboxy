//
//  ImageCanvasElement.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageCanvasElement : NSObject
{
    int _idImage;           //id de la imagen
    int _type;              //tipo de imagen
    NSString *_name;        //nombre de la imagen
    NSString *_fullPath;    //ruta de la imagen
    int _idCanvasElement;    //id de canvas
}

@property (nonatomic,assign)int idImage;
@property (nonatomic,assign)int type;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *fullPath;
@property (nonatomic,assign)int idCanvasElement;

-(id)initWithidImage:(int)idImage type:(int)type name:(NSString *)name fullPath:(NSString *)fullPath idCanvasElement:(int)idCanvasElement;

@end