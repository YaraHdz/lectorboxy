//
//  DragDropRelationship.h
//  LectorBoxes
//
//  Created by SWAPUASLP on 25/07/15.
//  Copyright (c) 2015 SWAPUASLP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DragDropRelationship : NSObject
{
    int _id;            //id de DragDrop
    int _idDragElement; //id de Drag
    int _idDropElement; //id de Drop
}

@property (nonatomic,assign)int idDragDrop;
@property (nonatomic,assign)int idDragElement;
@property (nonatomic,assign)int idDropElement;

-(id)initWithidDragDrop:(int)idDragDrop idDragElement:(int)idDragElement idDropElement:(int)idDropElement;

@end